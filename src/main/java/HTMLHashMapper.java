import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.Objects;

/*
* Класс создает HashMap на основе файлов в директории
* */

public class HTMLHashMapper {
    public static HashMap<String, String> dirToHashMap(String dirPath){
        HashMap<String, String> hashMap = new HashMap<>();
        File directory = new File(dirPath);
        for (final File fileEntry : Objects.requireNonNull(directory.listFiles())) {
            if (!fileEntry.isDirectory()) {
                try {
                    String content = Files.readString(fileEntry.toPath());
                    hashMap.put(fileEntry.getName(), content);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return hashMap;
    }
}
