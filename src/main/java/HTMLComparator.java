import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/*
* Класс сравнивает файлы в двух директориях
* */

public class HTMLComparator {
    private final HashMap<String, String> dayOne;
    private final HashMap<String, String> dayTwo;
    private final ArrayList<String> removed = new ArrayList<>();
    private final ArrayList<String> modified = new ArrayList<>();
    private final ArrayList<String> added = new ArrayList<>();

    public HTMLComparator(HashMap<String, String> dayOne, HashMap<String, String> dayTwo) {
        this.dayOne = dayOne;
        this.dayTwo = dayTwo;
    }

    void compare(){
        for (Map.Entry<String, String> mapEntry : dayOne.entrySet()) {
            Map.Entry<String, String> pair = mapEntry;
            if (!dayTwo.containsKey(pair.getKey())) {
                removed.add(pair.getKey());
            } else if (!(pair.getValue()).equals(dayTwo.get(pair.getKey()))) {
                modified.add(pair.getKey());
            }
        }
        checkAddedPages();
    };

    void checkAddedPages(){
        for (Map.Entry<String, String> mapEntry : dayTwo.entrySet()) {
            Map.Entry<String, String> pair = mapEntry;
            if (!dayOne.containsKey(pair.getKey())) {
                added.add(pair.getKey());
            }
        }
    }

    public ArrayList<String> getRemoved() {
        return removed;
    }

    public ArrayList<String> getModified() {
        return modified;
    }

    public ArrayList<String> getAdded() {
        return added;
    }
}
