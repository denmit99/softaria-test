import java.util.ArrayList;

/*
* Класс отвечает за формирование текста письма от системы мониторинга
* */

public class LetterMaker {
    public static String formMonitoringLetter(ArrayList<String> removed, ArrayList<String> added, ArrayList<String> modified){
        StringBuilder content = new StringBuilder();
        content.append("Здравствуйте, дорогая и.о. секретаря\n\n");
        content.append("За последние сутки во вверенных Вам сайтах произошли следующие изменения:\n\n");
        if(!removed.isEmpty()){
            content.append("Исчезли следующие страницы: ").append(removed).append("\n");
        }
        if(!added.isEmpty()){
            content.append("Появились следующие страницы: ").append(added).append("\n");
        }
        if(!modified.isEmpty()){
            content.append("Изменились следующие страницы: ").append(modified).append("\n");
        }
        content.append("\nС уважением, автоматизированная система мониторинга.");
        return content.toString();
    }
}
