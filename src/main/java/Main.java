import java.util.HashMap;

public class Main {

    public static void main(String[] args) {
        HashMap<String, String> yesterday = HTMLHashMapper.dirToHashMap("src/main/resources/yesterday");
        HashMap<String, String> today = HTMLHashMapper.dirToHashMap("src/main/resources/today");
        HTMLComparator htmlComparator = new HTMLComparator(yesterday, today);
        htmlComparator.compare();
        String content = LetterMaker.formMonitoringLetter(htmlComparator.getRemoved(), htmlComparator.getAdded(), htmlComparator.getModified());
        System.out.println(content);
    }
}
